﻿using UnityEngine;
using System.Collections;

public class bg : MonoBehaviour {
	public GameObject background;
	float speed = 4.0f;
	void Update () {
        transform.position =  Vector2.MoveTowards(transform.position, new Vector2(0, -1), Time.deltaTime * speed);
		if(transform.position.y <= 0)
		{
            transform.position = new Vector2(0, 30);
		}
	}
}
