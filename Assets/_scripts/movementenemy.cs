﻿using UnityEngine;
using System.Collections;

public class movementenemy : MonoBehaviour {
	//poruszanie przeciwnika w dół
    float speed = 7.0f;
	void Update () {
		transform.Translate (-Vector2.up * Time.deltaTime * speed);
		transform.Rotate(Vector3.up * Time.deltaTime, Space.World);
	}
}
