﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class collision : MonoBehaviour {
	public GameObject dead_menu;
	public int points = 0;
	public Text textpkt;
   
    void Awake()
    {
        Application.targetFrameRate = 30;
    }
	void Start()
	{
		//textpkt.GetComponent<Renderer>().sortingOrder = 5;
        textpkt.fontSize =  (int)(Screen.height / 22);
	}
	void Update()
	{
		textpkt.text = points.ToString();
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Enemy") 
		{
			dead_menu.SetActive(true);
			int a = PlayerPrefs.GetInt("Highscore");
			if(points > a)
			{
				PlayerPrefs.SetInt("Highscore", points);
				PlayerPrefs.Save();
			}
			Vector2 np = new Vector2(0,-50);
			transform.position = np;
		}
		else if(other.gameObject.tag == "point")
		{
			points++;
		}
	}
}
