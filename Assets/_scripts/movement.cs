﻿using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {
	public float playerSpeed = 3.0f;
    Vector3 fingerposition;
    void Start()
    {
        fingerposition = Vector3.zero;
    }
    
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel("menu");
        }
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (!(Physics.Raycast(ray, 100)))
            {
                fingerposition = Input.mousePosition;
                fingerposition = Camera.main.ScreenToWorldPoint(fingerposition);
                if (fingerposition.x >= 0)
                {
                    transform.Translate(Vector2.right * Time.deltaTime * playerSpeed * (fingerposition.x / fingerposition.x));
                }
                else if (fingerposition.x < 0)
                {
                    transform.Translate(-Vector2.right * Time.deltaTime * playerSpeed * (fingerposition.x / fingerposition.x));
                }
            }
        }
        else
            transform.Translate(Vector2.right * Time.deltaTime * playerSpeed * fingerposition.x);
    }

}